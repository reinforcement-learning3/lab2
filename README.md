# Lab 2 from [UC Berkeley Deep RL Bootcamp](https://sites.google.com/view/deep-rl-bootcamp/lectures)

## UC Berkeley Lectures

- [Deep Q Networks](https://www.youtube.com/watch?v=fevMOp5TDQs)
- [Policy Gradients](https://www.youtube.com/watch?v=S_gwYj1Q-44)
