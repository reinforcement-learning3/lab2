Grading of Lab2
===

**Positive**
Overall the lab was solved well, most of the necessary parts are there. The part I liked was that different learning rates were plotted for the simple linear regression problem in the `Introduction to Chainer` part even though this was not asked for.

**Negative**
However I was missing several things to give a perfect mark

1. Learning curves for the deep models are not visualized which makes interpretation less intuitive and harder to follow
2. The `extra_run` function does not really say what it is doing since there is missing documentation (docstring or markdown cell)
3. The `extra_run` implements the correlated gradients though also changes the batchsize. --> Comparison to the baseline harder since multiple parameters changed.
4. The `extra_run` only uses one digit and a conclusion is missing.
5. There is no answer to the `Does using a ConvNet improve performance (reduce overfitting?)` question.
6. The learning rate was not changed for the ConvNet

**Grading**

Since most necessary parts were performed but there is missing documentation and conclusion to the given questions i would give a **5.0** to this lab.
